package com.kloneworld.assigment_three;

/**
 * Created by kloneworld on 06/10/17.
 */

public class Candidate {

    private String Name;
    private String VideoKey;
    private String VideoURL;

    public Candidate (String name, String videoKey){
        Name= name;
        VideoKey = videoKey;
    }

    public String getVideoURL(){
            VideoURL = Constants.BaseURL+ Constants.Service + "?" + Constants.Key+ "="+ VideoKey;
        return VideoURL;
    }
}
